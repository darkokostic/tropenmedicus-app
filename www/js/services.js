angular.module('tropenmedicus.services', [])

    .service('LoginService', function ($http, $localStorage) {

        function checkIfLoggedIn() {

            if ($localStorage.isLoggedIn == false || $localStorage.isLoggedIn == undefined) {
                return false;
            } else {
                return true;
            }

        }

        function isValid() {
            var date = new Date();
            var ms = date.getTime();
            // var testDate = new Date("2018-12-31"); // Test For Account Validation }
            // var testMs = testDate.getTime(); //                                   }
            if ($localStorage.validUntil < ms) {
                return false;
            } else {
                return true;
            }
        }

        function login(username, password, onSuccess, onError) {

            $http.post('http://tropenmedicus.de/_api/user.php?password=' + password + '&user=' + username,
                {
                    headers: {'Content-Type': 'application/json'}
                }
            )
                .then(function (response) {

                    if (response.data.error == 0) {
                        $localStorage.isLoggedIn = true;
                        onSuccess();
                    } else {
                        $localStorage.isLoggedIn = false;
                        onError(response.data);
                    }

                }, function (response) {
                    onError(response.data);
                });

        }

        function logout() {

            $localStorage.isLoggedIn = false;

        }

        return {
            login: login,
            isValid: isValid,
            checkIfLoggedIn: checkIfLoggedIn,
            logout: logout
        }

    })

    .service('HttpGetService', function ($http) {

        function getCountries() {
            return $http.get('http://tropenmedicus.de/_api/c_and_d.php')
                .then(function (response) {
                    return response.data;
                })
                .catch(function (error) {
                    return error;
                })
        }

        function getOutbreak() {
            return $http.get('http://tropenmedicus.de/_api/outbreak.php')
                .then(function (response) {
                    return response.data;
                })
                .catch(function (error) {
                    return error;
                })
        }

        function getPdfsDate() {
            return $http.get('http://tropenmedicus.de/_api/update.php')
                .then(function (response) {
                    return response.data;
                })
                .catch(function (error) {
                    return error;
                })
        }

        return {
            getCountries: getCountries,
            getOutbreak: getOutbreak,
            getPdfsDate: getPdfsDate
        }

    });