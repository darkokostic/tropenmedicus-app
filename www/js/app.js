angular.module('tropenmedicus', ['ionic', 'ngCordova', 'ngStorage', 'pdfjsViewer', 'tropenmedicus.controllers', 'tropenmedicus.services'])

    .run(function ($ionicPlatform, $rootScope, $localStorage, $cordovaNetwork, HttpGetService, $ionicPopup) {
        $rootScope.internetConnected = true;
        // DISABLE HARDWARE BACK BUTTON ON POPUP
        $ionicPlatform.onHardwareBackButton(function () {
            if ($rootScope.alertPopup != null) {
                console.log("Hardware Back Button!");
                $rootScope.alertPopup.close();
                navigator.app.exitApp();
            }
        }, 100);

        $ionicPlatform.ready(function () {

            $rootScope.$watch(function () {
                return $localStorage.isLoggedIn;
            }, function (newVal, oldVal) {
                if (newVal == true) {
                    $rootScope.loggedIn = true;
                } else {
                    $rootScope.loggedIn = false;
                }
            });

            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }

            if (window.StatusBar) {
                StatusBar.styleDefault();
            }

            $rootScope.downloadOutbreak = function(response) {
                $rootScope.newOutbreakPdf = response;
                $rootScope.alertPopup = $ionicPopup.alert({
                    title: 'Aktuelle Outbreak News vorhanden. Lesen Sie diese unter dem Menupunkt "Outbreak News".',
                    templateUrl: 'templates/popups/download-outbreak-popup.html',
                    buttons: null
                });
            };

            $rootScope.checkOutbreakNews = function(canGo) {
                if($localStorage.isLoggedIn && $rootScope.internetConnected) {
                    HttpGetService.getOutbreak()
                        .then(function (response) {
                            console.log(response);
                            if($localStorage.outbreakPdf) {
                                if($localStorage.outbreakPdf.time != response.time || $localStorage.outbreakPdf.size != response.size || $localStorage.outbreakPdf.file != response.file) {
                                    $rootScope.canGoToPdf = canGo;
                                    $rootScope.downloadOutbreak(response);
                                }
                            } else {
                                $rootScope.canGoToPdf = canGo;
                                $rootScope.downloadOutbreak(response);
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }
            };

            $rootScope.checkPdfsDate = function() {
                if($localStorage.isLoggedIn && $rootScope.internetConnected) {
                    HttpGetService.getPdfsDate()
                        .then(function (response) {
                            var date = new Date(response.date_of_pdf);
                            var ms = date.getTime();
                            // var testDate = new Date("2016-12-31"); // Test For New Pdfs Date }
                            // var testMs = testDate.getTime(); //                              }
                            if ($localStorage.pdfsDate != ms) {
                                var message = 'Es sind neue PDFs vorhanden. Bitte melden Sie sich neu an, um die Daten herunterzuladen!';
                                $rootScope.logoutIsValid(message);
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }
            };

            $rootScope.checkOutbreakNews(false);
            $rootScope.checkPdfsDate();

            // CHECK INTERNTET CONNECTION
            $rootScope.$on('$cordovaNetwork:offline', function (event, networkState) {
                if (!$rootScope.internetConnected) return;
                $rootScope.internetConnected = false;
            });

            $rootScope.$on('$cordovaNetwork:online', function (event, networkState) {
                if ($rootScope.internetConnected) return;
                $rootScope.internetConnected = true;
                $rootScope.checkOutbreakNews();
                $rootScope.checkPdfsDate();
            });

        });
    })

    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })

            .state('app.home', {
                url: '/',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/home.html',
                        controller: 'HomeCtrl'
                    }
                }
            })

            .state('app.countries', {
                url: '/countries',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/countries.html',
                        controller: 'CountriesCtrl'
                    }
                }
            })

            .state('app.diseases', {
                url: '/diseases',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/diseases.html',
                        controller: 'DiseasesCtrl'
                    }
                }
            })

            .state('app.pdfview', {
                url: '/pdf/:pdfName',
                params: {
                    pdfName: null
                },
                views: {
                    'menuContent': {
                        templateUrl: 'templates/pdf-view.html',
                        controller: 'PdfViewCtrl'
                    }
                }
            })

            .state('app.login', {
                url: '/login',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/login.html',
                        controller: 'LoginCtrl'
                    }
                }
            });
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/');
    });
