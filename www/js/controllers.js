angular.module('tropenmedicus.controllers', [])

    .controller('AppCtrl', function ($scope, $rootScope, $state, LoginService, $ionicHistory, $ionicLoading, $localStorage, HttpGetService, $cordovaFileTransfer) {

        $scope.logout = function () {
            LoginService.logout();
            $state.go('app.home');
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $ionicLoading.show({template: 'Erfolgreich abgemeldet!', noBackdrop: true, duration: 1000});
        };

        $scope.removeDirectory = function () {
            var directoryToRemove = window.cordova.file.dataDirectory + '/pdfs/';
            window.resolveLocalFileSystemURL(directoryToRemove, function (dirEntry) {
                function successHandler() {
                    console.log('Directory deleted successfully');
                }
                function errorHandler() {
                    console.log('There is some error while deleting directory');
                }
                dirEntry.removeRecursively(successHandler, errorHandler);
            });
        };

        $rootScope.logoutIsValid = function (messageType) {
            LoginService.logout();
            $state.go('app.login');
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $ionicLoading.show({template: messageType, noBackdrop: true, duration: 3000});
            $scope.removeDirectory();
        };

        $scope.go = function (pdfName) {
            if (LoginService.isValid()) {
                if (LoginService.checkIfLoggedIn() == true) {
                    $state.go('app.pdfview', {'pdfName': pdfName});
                } else {
                    $state.go('app.login');
                }
            } else {
                var message = 'Ihre Zugangsdaten sind ungültig';
                $rootScope.logoutIsValid(message);
            }
        };

        $scope.checkOutbreakNews = function() {
            if($localStorage.outbreakPdf) {
                $state.go('app.pdfview', {'pdfName': $localStorage.outbreakPdf.file});
            } else if($rootScope.internetConnected) {
                $rootScope.checkOutbreakNews(true);
            } else {
                $ionicLoading.show({template: 'Kein Internet!', noBackdrop: true, duration: 1000});
            }
        };

        $scope.goOutbreak = function() {
            if (LoginService.isValid()) {
                if (LoginService.checkIfLoggedIn() == true) {
                    $scope.checkOutbreakNews();
                } else {
                    $state.go('app.login');
                }
            } else {
                var message = 'Ihre Zugangsdaten sind ungültig';
                $rootScope.logoutIsValid(message);
            }
        };

        $scope.goCountries = function() {
            if (LoginService.isValid()) {
                $state.go('app.countries');
            } else {
                var message = 'Ihre Zugangsdaten sind ungültig';
                $rootScope.logoutIsValid(message);
            }
        };

        $scope.goDiseases = function() {
            if (LoginService.isValid()) {
                $state.go('app.diseases');
            } else {
                var message = 'Ihre Zugangsdaten sind ungültig';
                $rootScope.logoutIsValid(message);
            }
        };

    })

    .controller('HomeCtrl', function ($scope, $rootScope, LoginService, $state, $ionicHistory) {

        $ionicHistory.clearHistory();

        $scope.go = function (pdfName) {
            if (LoginService.isValid()) {
                if (LoginService.checkIfLoggedIn() == true) {
                    $state.go('app.pdfview', {'pdfName': pdfName});
                } else {
                    $state.go('app.login');
                }
            } else {
                var message = 'Ihre Zugangsdaten sind ungültig';
                $rootScope.logoutIsValid(message);
            }
        };
    })

    .controller('CountriesCtrl', function ($scope, $rootScope, $localStorage, LoginService, $state) {
        $scope.countries = $localStorage.countries;
        $scope.go = function (pdfName) {
            if (LoginService.isValid()) {
                if (LoginService.checkIfLoggedIn() == true) {
                    $state.go('app.pdfview', {'pdfName': pdfName});
                } else {
                    $state.go('app.login');
                }
            } else {
                var message = 'Ihre Zugangsdaten sind ungültig';
                $rootScope.logoutIsValid(message);
            }
        };
    })

    .controller('DiseasesCtrl', function ($scope, $rootScope, $localStorage, LoginService, $state) {
        $scope.diseases = $localStorage.diseases;
        $scope.go = function (pdfName) {
            if (LoginService.isValid()) {
                if (LoginService.checkIfLoggedIn() == true) {
                    $state.go('app.pdfview', {'pdfName': pdfName});
                } else {
                    $state.go('app.login');
                }
            } else {
                var message = 'Ihre Zugangsdaten sind ungültig';
                $rootScope.logoutIsValid(message);
            }
        };
    })

    .controller('PdfViewCtrl', function ($scope, $stateParams, LoginService, $ionicHistory, $state, $ionicLoading, $sce, $timeout) {

        var pdfName = $stateParams.pdfName;
        var targetPath = "";
        $scope.pdfUrl = '';

        targetPath = window.cordova.file.dataDirectory + '/pdfs/' + pdfName;

        window.resolveLocalFileSystemURL(targetPath, function () {
            console.log(targetPath);
            $scope.pdfUrl = targetPath;
        }, function () {
            console.log("File Doesn't Exist");
            LoginService.logout();
            $state.go('app.home');
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $ionicLoading.show({template: 'Bitte melden Sie sich nochmals an!', noBackdrop: true, duration: 1000});
        });

        $scope.httpHeaders = {'Access-Control-Allow-Origin': '*'};

    })

    .controller('LoginCtrl', function ($scope, $rootScope, LoginService, $state, $ionicHistory, $ionicLoading, $ionicPopup, $localStorage, HttpGetService) {

        $scope.login = function (username, password) {
            HttpGetService.getPdfsDate()
                .then(function (response) {
                    var date = new Date(response.date_of_pdf);
                    var ms = date.getTime();
                    $localStorage.pdfsDate = ms;
                })
                .catch(function (error) {
                    console.log(error);
                })
            if (username != undefined && username != '' && password != undefined && password != '') {
                LoginService.login(username, password,
                    function () {
                        $ionicLoading.show({template: 'Erfolgreich angemeldet!', noBackdrop: true, duration: 1000});

                        $rootScope.alertPopup = $ionicPopup.alert({
                            title: 'Download... Bitte warten',
                            templateUrl: 'templates/popups/download-popup.html',
                            buttons: null
                        });
                    },
                    function (response) {
                        if (response != null) {
                            $ionicLoading.show({
                                template: 'Benutzername oder Passwort falsch!',
                                noBackdrop: true,
                                duration: 1000
                            });
                        } else {
                            $ionicLoading.show({template: 'Kein Internet!', noBackdrop: true, duration: 1000});
                        }
                    });
            } else {
                $ionicLoading.show({
                    template: 'Bitte Benutzernamen und Passwort eingeben!',
                    noBackdrop: true,
                    duration: 1000
                });
            }
        }

    })

    .controller('PopUpCtrl', function ($scope, $rootScope, $cordovaFileTransfer, $timeout, $ionicLoading, $ionicHistory, $state, HttpGetService, $localStorage) {

        $scope.pdfsArray = [
            {
                url: 'http://tropenmedicus.de/_api/_pdf/0_titel.pdf',
                file: '0_titel.pdf',
                name: 'Titel',
                type: 'main_pdf'
            },
            {
                url: 'http://tropenmedicus.de/_api/_pdf/1_umschlag.pdf',
                file: '1_umschlag.pdf',
                name: 'Umschlag',
                type: 'main_pdf'
            },
            {
                url: 'http://tropenmedicus.de/_api/_pdf/2_laender.pdf',
                file: '2_laender.pdf',
                name: 'Leander',
                type: 'main_pdf'
            },
            {
                url: 'http://tropenmedicus.de/_api/_pdf/3_impfungen.pdf',
                file: '3_impfungen.pdf',
                name: 'Impfungen',
                type: 'main_pdf'
            },
            {
                url: 'http://tropenmedicus.de/_api/_pdf/4_malaria.pdf',
                file: '4_malaria.pdf',
                name: 'Malaria',
                type: 'main_pdf'
            },
            {
                url: 'http://tropenmedicus.de/_api/_pdf/5_krankheiten.pdf',
                file: '5_krankheiten.pdf',
                name: 'Kranktheiten',
                type: 'main_pdf'
            },
            {
                url: 'http://tropenmedicus.de/_api/_pdf/6_hinweise.pdf',
                file: '6_hinweise.pdf',
                name: 'Hinweise',
                type: 'main_pdf'
            }];

        HttpGetService.getCountries()
            .then(function (response) {
                console.log(response);

                $scope.countries = [];
                $scope.diseases = [];

                angular.forEach(response.aCountry, function (country, value) {
                    var newCountry = {
                        url: response.folder_countries + country.file,
                        file: country.file,
                        name: country.country,
                        type: 'country'
                    };
                    $scope.pdfsArray.push(newCountry);
                    $scope.countries.push(newCountry);
                });

                angular.forEach(response.aDisease, function (disease, value) {
                    var newDisease = {
                        url: response.folder_disease + disease.file,
                        file: disease.file,
                        name: disease.name,
                        type: 'disease'
                    };
                    $scope.pdfsArray.push(newDisease);
                    $scope.diseases.push(newDisease);
                });

                $localStorage.countries = $scope.countries;
                $localStorage.diseases = $scope.diseases;

                download($scope.pdfsArray[0], 0);
            })
            .catch(function (error) {
                console.log(error);
            });

        function download(pdf, counter) {
            console.log($scope.pdfsArray.length - 1);
            if (counter > $scope.pdfsArray.length - 1) {

                $ionicLoading.show({
                    template: 'Alle Dateien erfolgreich übernommen!',
                    noBackdrop: true,
                    duration: 1000
                });
                $ionicHistory.nextViewOptions({
                    disableBack: true
                });
                $state.go('app.home');
                $rootScope.alertPopup.close();

            } else {
                $scope.counter = counter;

                $scope.downloadProgress = 0;

                var url = pdf.url;
                var targetPath = window.cordova.file.dataDirectory + '/pdfs/' + pdf.file;
                var trustHosts = true;
                var options = {};

                console.log(pdf.file);
                window.resolveLocalFileSystemURL(targetPath, function () {
                    console.log("File Exist");
                    $scope.downloadProgress = 100;
                    counter++;
                    download($scope.pdfsArray[counter], counter);
                }, function () {
                    console.log("File Doesn't Exist");
                    $cordovaFileTransfer.download(url, targetPath, options, trustHosts)
                        .then(function (result) {
                            counter++;
                            download($scope.pdfsArray[counter], counter);
                        }, function (err) {
                            console.log(err);
                        }, function (progress) {
                            $timeout(function () {
                                $scope.downloadProgress = (progress.loaded / progress.total) * 100;
                            });
                        });
                });
            }
        }
    })

    .controller('OutbreakPopUpCtrl', function ($scope, $rootScope, $cordovaFileTransfer, $timeout, $ionicLoading, $ionicHistory, $state, $localStorage) {

        $scope.counter = 1;
        $scope.downloadProgress = 0;

        var url = $rootScope.newOutbreakPdf.url;
        var targetPath = window.cordova.file.dataDirectory + '/pdfs/' + $rootScope.newOutbreakPdf.file;
        var trustHosts = true;
        var options = {};

        window.resolveLocalFileSystemURL(targetPath, function () {
            console.log("File Exist");

            $localStorage.outbreakPdf = $rootScope.newOutbreakPdf;

            $scope.downloadProgress = 100;

            $ionicLoading.show({
                template: 'Alle Dateien erfolgreich übernommen!',
                noBackdrop: true,
                duration: 1000
            });

            $rootScope.alertPopup.close();

            if($rootScope.canGoToPdf) {
                $state.go('app.pdfview', {'pdfName': $localStorage.outbreakPdf.file});
            }

        }, function () {
            console.log("File Doesn't Exist");
            $cordovaFileTransfer.download(url, targetPath, options, trustHosts)
                .then(function (result) {
                    $localStorage.outbreakPdf = $rootScope.newOutbreakPdf;
                    setTimeout(function () {
                            $ionicLoading.show({
                                template: 'Alle Dateien erfolgreich übernommen!',
                                noBackdrop: true,
                                duration: 1000
                            });
                            $rootScope.alertPopup.close();
                            if($rootScope.canGoToPdf) {
                                $state.go('app.pdfview', {'pdfName': $localStorage.outbreakPdf.file});
                            }
                        }, 3000);
                    

                }, function (err) {
                    console.log(err);
                }, function (progress) {
                    $timeout(function () {
                        $scope.downloadProgress = (progress.loaded / progress.total) * 100;
                    });
                });
        });

    });
